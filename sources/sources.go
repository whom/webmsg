package sources

import (
    "github.com/spf13/viper"
    "git.callpipe.com/aidan/webmsg/log"
    "git.callpipe.com/aidan/webmsg/models"
)

var Recv chan models.Message
var Sources = map[string]Source{}

/*
 * Source:
 * This interface defines how webmsg interoperates with external messaging systems
 * In this interface is defined the minumum functionality needed to 
 * Send and Recieve messages over an external system.
 *
 * Send(message): Sends a message. Yep.
 *
 * Recieves by embedding the Recv channel into the source.
 * The source is in charge of spinning up a go routine to handle incoming messages.
 * The source is also in charge of storing new messages in the database.
 */

type Source interface {
    Init(buf chan models.Message) error
    Send(models.Message, models.Contact) error
}

func InitializeEndpoints() {
    Recv = make(chan models.Message, viper.GetInt("MaxGlobalMsgBufferLen"))
    Sources["loopback"] = LoopbackSource{nil}

    for k, v := range(Sources) {
        err := v.Init(Recv)
        if err != nil {
            log.Log("Source " + k + " failed to initialize", "sources")
            log.Log(err.Error(), "sources")
            log.Log("Removing it from configuration", "sources")
            delete(Sources, k)
        } else {
            log.Log("Initialized " + k + " message source", "sources")
        }
    }

    log.Log("Initialized all message sources", "sources")
}
