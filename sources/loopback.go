package sources

import (
    "errors"
    "git.callpipe.com/aidan/webmsg/models"
)

type LoopbackSource struct {
    Inner chan models.Message
}

func (l LoopbackSource) Init(buf chan models.Message) error {
    l.Inner = buf
    return nil
}

func (l LoopbackSource) Send(msg models.Message, acct models.Contact) error {
    if len(msg.Recipients) != 1 || msg.Recipients[1].ContactID != acct.ContactID {
        return errors.New("Not a real loopback message. Invalid recipients")
    }

    l.Inner <- msg
    return nil
}
