package auth

import (
    "fmt"
    "time"
    "crypto/ecdsa"
    "github.com/spf13/viper"
    "github.com/go-ldap/ldap/v3"
    "github.com/dgrijalva/jwt-go"
    "git.callpipe.com/aidan/webmsg/log"
    "git.callpipe.com/aidan/webmsg/models"
    "git.callpipe.com/aidan/webmsg/storage"
)

type Claim struct {
    Username string `json:"username"`
    jwt.StandardClaims
}

var LDAP ldap.Conn
var public *ecdsa.PublicKey
var private *ecdsa.PrivateKey

func SetupAuth(ldap *ldap.Conn, priv *ecdsa.PrivateKey, pub *ecdsa.PublicKey) {
    public = pub
    private = priv

    if ldap == nil {
        log.Panic("LDAP Connection is nil, cannot initialize")
    }

    LDAP = *ldap


}

func VerifyUser(user string) *models.User {
    var ret models.User
    storage.DB.Where("user_id = ?", user).First(&ret)
    if ret.UserID == user {
        return &ret
    }

    return nil
}

// returns user if logged in else ""
func ValidateJWT(token string) string {
    claim := &Claim{}

    tkn, err := jwt.ParseWithClaims(token, claim, 
        func (token *jwt.Token) (interface{}, error) {
            return public, nil
        })
    if err != nil {
        log.Log(err.Error(), "JWT")
        return ""
    }

    if !tkn.Valid {
        log.Log("Bad user login attempt: "+ claim.Username, "JWT")
        return ""
    }

    // catch expired
    if time.Unix(claim.ExpiresAt, 0).Sub(time.Now()) < 0 * time.Second {
        return ""
    }

    r := VerifyUser(claim.Username)
    if r == nil {
        return ""
    }

    return r.UserID
}

func MkJWT(user string) string {
    // TODO: make this a configurable variable
    expires := time.Now().Add(2 * time.Hour)
    claims := &Claim{
        Username: user,
        StandardClaims: jwt.StandardClaims{
            ExpiresAt: expires.Unix(),
        },
    }

    // ECDSA with SHA-512
    tok := jwt.NewWithClaims(jwt.SigningMethodES512, claims)
    tokstr, err := tok.SignedString(private)
    if err != nil {
        log.Log(err.Error(), "JWT")
        return "" // will force user to log in again
    }

    return tokstr
}

func Authenticate(username string, password string) bool {
    // Escape Params
    username = ldap.EscapeFilter(username)
    password = ldap.EscapeFilter(password)

    searchRequest := ldap.NewSearchRequest(
        viper.GetString("LDAPBase"),
        ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 1, 30, false,
        fmt.Sprintf("(cn=%s)", username),
        []string{"dn"},
        nil,
    )

    rec, err := LDAP.Search(searchRequest)
    if err != nil {
        log.Log("Error searching for user", "LDAP")
        log.Log(err.Error(), "LDAP")
        return false
    }

    if len(rec.Entries) != 1 {
        // this was in reference code
        // do some research and find if we need to fail here
        log.Log(fmt.Sprintf("%d users found, cannot auth", len(rec.Entries)), "LDAP")
        return false
    }

    dn := rec.Entries[0].DN
    err = LDAP.Bind(dn, password)
    if err != nil {
        log.Log("Failed login attempt: " + username, "LDAP")
        return false
    }

    err = LDAP.Bind("cn=" + viper.GetString("LDAPClientUser") + "," +
                    viper.GetString("LDAPBase"),
                    viper.GetString("LDAPClientPasswd"))
    if err != nil {
        // Panic here to avoid future auth requests with non configured creds
        log.Panic("Failed to re-bind with assigned LDAP Client creds!")
    }

    return true
}

