/*
 *This code contains logic for the login page.
 */

package pages

import (
    "time"
    "net/http"
    "git.callpipe.com/aidan/webmsg/log"
    "git.callpipe.com/aidan/webmsg/auth"
)

type LoginPageData struct {
    Flash bool
    Msg string
}

func LoginPageHandler(w http.ResponseWriter, r *http.Request) {
     if r.Method == "GET" {
        err := html.ExecuteTemplate(w, "loginpage.html", nil)
        if err != nil {
            log.Log(err.Error(), "web")
            html.ExecuteTemplate(w, "error.html", nil)
        }

    } else if r.Method == "POST" {
        // Get Form Data
        err := r.ParseForm()
        if err != nil {
            log.Log(err.Error(), "web")
            html.ExecuteTemplate(w, "error.html", nil)
        }

        user := r.FormValue("user")
        pass := r.FormValue("password")

        ok := auth.Authenticate(user, pass)
        if !ok {
            html.ExecuteTemplate(w, "loginpage.html", 
                LoginPageData{Flash: true, Msg: "Invalid username or password"})

        } else {
            http.SetCookie(w, &http.Cookie{
                Name: "session",
                Value: auth.MkJWT(user),
                // TODO: Make configurable via viper
                Expires: time.Now().Add(2 * time.Hour),
            })
            http.Redirect(w, r, "/contacts", 301)
            return
        }

    } else {
        http.Redirect(w, r, "/badrequest", 401)
    }
}

