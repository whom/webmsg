/*
 * This code contains logic specific to the contact page
 */

package pages

import (
    "net/http"
    "git.callpipe.com/aidan/webmsg/auth"
    "git.callpipe.com/aidan/webmsg/models"
    "git.callpipe.com/aidan/webmsg/storage"
)

func ContactPageHandler(w http.ResponseWriter, r *http.Request) {
    cookie, err := r.Cookie("session")
    if err != nil {
        http.Redirect(w, r, "/login", 301)
        return
    }

    user := auth.ValidateJWT(cookie.Value)
    if user == "" {
        http.Redirect(w, r, "/login", http.StatusUnauthorized)
        return
    }

    var userModel models.User
    storage.DB.Where("UserID = ?", user).Take(&userModel)
    html.ExecuteTemplate(w, "contacts.html", userModel.Contacts)
}
