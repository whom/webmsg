/*
 * This code contains abstractions for Pages
 * Additionally, this code binds page handlers to endpoints
 */

package pages

import (
    "fmt"
    "net/http"
    "html/template"
    "github.com/spf13/viper"
    "git.callpipe.com/aidan/webmsg/log"
)

type Page struct {
    Title string
    Templates []string
    NeedsAuth bool
}

var html *template.Template

func InitTemplates() {
    assetDir := viper.GetString("InstallDir")
    if assetDir == "" {
        log.Panic("installdir not set in config file!")
    }

    var err error
    html, err = html.ParseGlob(assetDir + "/pages/templates/*")
    if err != nil {
        log.Panic(fmt.Sprintf("Cannot parse templates: %s", err))
    }

    http.HandleFunc("/", ContactPageHandler)
    http.HandleFunc("/login", LoginPageHandler)
    http.HandleFunc("/contacts", ContactPageHandler)
    http.HandleFunc("/license", LicensePageHandler)
    // TODO: message
    log.Log("Finished digesting templates, url handlers registered", "Web")
}


