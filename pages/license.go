/*
 * This code contains logic specific to the license page
 */

package pages

import (
    "net/http"
)

func LicensePageHandler(w http.ResponseWriter, r *http.Request) {
    html.ExecuteTemplate(w, "license.html", nil)
}
