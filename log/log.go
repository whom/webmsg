package log

import (
    "os"
    "fmt"
    "time"
    "github.com/spf13/viper"
)

var logfile *os.File

func InitLog() {
    log := viper.GetString("LogFile")
    if _, err := os.Stat(log); err != nil {
        logfile, err = os.Create(log)
        if err != nil {
            panic(err.Error())
        }

    } else {
        err = os.Rename(log, log+".old")
        if err != nil {
            panic(err.Error())
        }

        logfile, err = os.Create(log)
        if err != nil {
            panic(err.Error())
        }
    }

    Log("Logging Initialized", "LOG")
}

func StopLog() {
    logfile.Close()
}

func Log(message string, module string) {
    line := fmt.Sprintf("[%10s] %12d: %s\n", module, time.Now().Unix(), message)
    logfile.Write([]byte(line))
    fmt.Printf(line)
}

func Panic(message string) {
    Log(message, "FATAL")
    StopLog()
    panic(message)
}
