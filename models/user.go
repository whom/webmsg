package models

import (
    "github.com/jinzhu/gorm"
)

/* User
 * Represents a client who uses webmsg.
 * Owns multiple Contacts.
 */

type User struct {
    gorm.Model
    UserID   string    `gorm:"primary_key"`
    Contacts []Contact `gorm:"foreignkey:ID"`
}
