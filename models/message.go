package models

import (
    "time"
    "github.com/jinzhu/gorm"
)

/* Message
 * Holds a message sent by an Contacts.
 * (TODO) Content will need to also contain Multimedia at some point
 * Many to Many relationship with Contacts 
 */

type Message struct {
    gorm.Model
    MessageID  int       `gorm:"AUTO_INCREMENT;not null;primary_key"`
    Sender     Contact   `gorm:"foreignkey:ContactID"`
    Content    string
    Recipients []Contact `gorm:"many2many:MessageRecipients;"`
    TimeAware  time.Time
    GroupID    Group     `gorm:"foreignkey:GroupID"`
}
