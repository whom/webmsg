package models

import (
    "github.com/jinzhu/gorm"
)

/* Group
 * Holds group names that can be attached to messages.
 * Many to many with contacts, one to many with messages.
 */

type Group struct {
    gorm.Model
    GroupID int       `gorm:AUTO_INCREMENT;not null;primary_key"`
    Name    string
    Members []Contact `gorm:"many2many:GroupMembers"`
}
