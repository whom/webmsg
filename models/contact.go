package models

import (
    "github.com/jinzhu/gorm"
)

/* Contact
 * Represents a person that can send or recieve messages over a platform.
 * (for local accounts owned by a user)
 */

type Contact struct {
    gorm.Model
    ContactID   int       `gorm:"AUTO_INCREMENT;not null;primary_key"`
    Platform    string    `gorm:"not null"`
    Username    string    `gorm:"not null"` // TODO: Unique
    Displayname string
    Recieved    []Message `gorm:"many2many:MessageRecipients;"`
    Groups      []Group   `gorm:"many2many:GroupMembers;"`
}

