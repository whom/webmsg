package config

import (
    "os"
    "time"
    "os/user"
    "math/rand"
    "github.com/spf13/viper"
)

var SaltCharset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
var Random = rand.New(rand.NewSource(time.Now().UnixNano()))

func getRandomString() string {
    str := make([]byte, 16)
    for i := range str {
        str[i] = SaltCharset[Random.Intn(len(SaltCharset))]
    }

    return string(str)
}

func homeDir() string {
    usr, err := user.Current()
    if err != nil {
        println("Couldnt get home dir")
        println(err.Error)
        return ""

    } else {
        return usr.HomeDir
    }
}

func SetViperDefaults() error {
    println("No config, generating one.")
    target := homeDir() + "/.webmsg"
    if _, err := os.Stat(target); os.IsNotExist(err) {
        err = os.Mkdir(target, 777) // TODO: Be less generous
        if err != nil {
            println("Couldnt generate config.")
            println(err.Error())
        }
    }

    // TODO: Document these and all variables
    viper.SetDefault("DatabasePort",   "5432")
    viper.SetDefault("DatabaseUser", "webmsg")
    viper.SetDefault("Database",     "webmsg")
    viper.SetDefault("DatabaseHost", "localhost")
    viper.SetDefault("DatabasePasswd", getRandomString())
    viper.SetDefault("LogFile", homeDir()+"/.webmsg/log")
    viper.SetDefault("MaxGlobalBufferLen", 1000)
    viper.SetDefault("LDAPServer", "localhost")
    viper.SetDefault("LDAPClientPasswd", getRandomString())
    viper.SetDefault("LDAPPort",   "389")
    viper.SetDefault("LDAPClientUser", "readonlyuser")
    viper.SetDefault("LDAPBase", "dc=example,dc=com") // what server to connect to 
    viper.SetDefault("InstallDirectory", homeDir()+"/go/src/git.callpipe.com/aidan/webmsg/")
    viper.SetDefault("JWTPubKey", homeDir()+"/.webmsg/pub.key")
    viper.SetDefault("JWTPrivKey", homeDir()+"/.webmsg/priv.key")

    return viper.WriteConfigAs(target + "/webmsg.yaml")
}

func GetViperConfig() error {
    viper.SetConfigType("yaml")
    viper.SetConfigName("webmsg.yaml")

    // Config paths to look for Viper config
    viper.AddConfigPath(".")
    viper.AddConfigPath(homeDir()+"/.webmsg")
    return viper.ReadInConfig()
}
