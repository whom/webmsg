package storage

import (
    "fmt"
    "os/exec"
    "github.com/spf13/viper"
    "github.com/jinzhu/gorm"
    _ "github.com/jinzhu/gorm/dialects/postgres"
    "git.callpipe.com/aidan/webmsg/models"
    "git.callpipe.com/aidan/webmsg/log"
)

var DB *gorm.DB

func InitDB() error {
    var err error
    err = nil

    // TODO: Refactor these Gets into config module
    port := viper.Get("DatabasePort")
    user := viper.Get("DatabaseUser")
    dtb  := viper.Get("Database")
    host := viper.Get("DatabaseHost")
    pass := viper.Get("DatabasePasswd")
    if port == "" ||
       user == "" ||
       dtb  == "" ||
       host == "" ||
       pass == "" {

        log.Panic("Make sure the following are defined in your config:"+
            "DatabasePort, DatabaseUser, Database, DatabaseHost, DatabasePasswd")
    }

    connection := fmt.Sprintf("host=%s user=%s dbname=%s port=%s sslmode=verify-full password=%s",
        host, user, dtb, port, pass)
    DB, err = gorm.Open("postgres", connection)
    if DB == nil || err != nil {
        var filepath string
        // TODO: Test this better!
        println(err.Error())
        println("Falling back to starting user local postgres server. "+
                "Please enter a filepath for postgres config: ")
        fmt.Scanln(&filepath)

        pg := func() {
            err := exec.Command("postgres-server -D " + filepath).Run()
            if err != nil {
                println("postgres did not start. please file a bug report. expect application breakage")
                println(err.Error())
            }
        }

        go pg()
        err = InitDB()
    }
    log.Log("Connected to Database", "Storage")

    DB.AutoMigrate(&models.Message{})
    DB.AutoMigrate(&models.Contact{})
    DB.AutoMigrate(&models.Group{})
    DB.AutoMigrate(&models.User{})
    log.Log("Migrations finished.", "Storage")
    return err
}

func TeardownDB() {
    DB.Close()
}
