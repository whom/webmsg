package api

import (
    "net/http"
    "encoding/json"
    "git.callpipe.com/aidan/webmsg/auth"
    "git.callpipe.com/aidan/webmsg/models"
    "git.callpipe.com/aidan/webmsg/storage"
)

func getHistoryHandler(w http.ResponseWriter, r *http.Request) {
    /* Method: GET
     * Data: JSON encoded HistoryRequest
     * Return: array of JSON encoded messages
     *
     * This one is the first endpoint called. 
     * Trigger a db query for all messages between a user and a contact
     * Spawn a background thread to continually check user's accounts
     */
    // TODO: Streaming API
    // TODO: Group Messages
    var request HistoryRequest

    err := json.NewDecoder(r.Body).Decode(&request)
    if err != nil {
        http.Error( w, err.Error(), http.StatusBadRequest)
        return
    }

    user := auth.ValidateJWT(string(request.AuthToken))
    if user == "" {
        http.Error(w, err.Error(), http.StatusUnauthorized)
    }

    // TODO: finish me ....
}

func getMsgHandler(w http.ResponseWriter, r *http.Request) {
    /* Method GET
     * Data: JSON encoded HistoryRequest
     * Return: array of JSON encoded messages
     *
     * Takes a JWT Token 
     * Returns new messages to requester in JSON format
     * Marks messages as read
     */
    var request HistoryRequest

    err := json.NewDecoder(r.Body).Decode(&request)
    if err != nil {
        http.Error( w, err.Error(), http.StatusBadRequest)
        return
    }

    user := auth.ValidateJWT(string(request.AuthToken))
    if user == "" {
        http.Error(w, err.Error(), http.StatusUnauthorized)
    }

    // TODO: finsh me ....
}

func sendMsgHandler(w http.ResponseWriter, r *http.Request) {
    /* Method: POST
     * Data: JSON encoded SendRequest
     * Return: error message, possibly
     *
     * Takes a JWT Token and JSON that matches message format
     * Validates the JWT Token and stores the message contents as a new message
     * Finally, calls correct API to send message
     */
    var request SendRequest

    err := json.NewDecoder(r.Body).Decode(&request)
    if err != nil {
        http.Error( w, err.Error(), http.StatusBadRequest)
        return
    }

    user := auth.ValidateJWT(string(request.AuthToken))
    if user == "" {
        http.Error(w, err.Error(), http.StatusUnauthorized)
    }

    // TODO: finish me ....
}

func newContactHandler(w http.ResponseWriter, r *http.Request) {
    /* Method: POST
     * Data: JSON encoded NewContactRequest
     * Return: error message, possibly
     *
     * Takes a JWT Token and json that matches a contact
     * Stores the contact in the database
     */
    var request NewContactRequest

    err := json.NewDecoder(r.Body).Decode(&request)
    if err != nil {
        http.Error( w, err.Error(), http.StatusBadRequest)
        return
    }

    user := auth.ValidateJWT(string(request.AuthToken))
    if user == "" {
        http.Error(w, err.Error(), http.StatusUnauthorized)
    }

    var contact models.Contact
    contact.Platform = request.Platform
    contact.Username = request.Username
    contact.Displayname = request.Displayname

    storage.DB.Create(&contact)
    http.Header().Set("Webmsg", "Contact created")
    http.WriteHeader(200)
}

func RegisterAPIHandlers() {
    http.HandleFunc("/history", getHistoryHandler)
    http.HandleFunc("/getmsg",  getMsgHandler )
    http.HandleFunc("/sendmsg", sendMsgHandler)
    http.HandleFunc("/newcontact", newContactHandler)
}

type BaseRequest struct {
    AuthToken []byte `json:"auth_token"`
}

// TODO: Group ID for group messages
type HistoryRequest struct {
    *BaseRequest
    Recipient   string `json: "recipient"`
}

type SendRequest struct {
    *BaseRequest
    Recipients   string  `json:"recipients"` // list of Contact IDs
    Content      string  `json:"content"`    // text of message
}

type NewContactRequest struct {
    *BaseRequest
    Platform    string  `json:"platform"`    // service the user operates on
    Username    string  `json:"username"`    // unique identifier used by target platform
    Displayname string  `json:"displayname"` // custom name the user sets
}
