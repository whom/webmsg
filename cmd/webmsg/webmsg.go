package main

import (
    "net/http"
    "crypto/tls"
    "crypto/rand"
    "crypto/ecdsa"
    "crypto/elliptic"
    "github.com/spf13/viper"
    "github.com/go-ldap/ldap/v3"
    "git.callpipe.com/aidan/webmsg/api"
    "git.callpipe.com/aidan/webmsg/log"
    "git.callpipe.com/aidan/webmsg/auth"
    "git.callpipe.com/aidan/webmsg/pages"
    "git.callpipe.com/aidan/webmsg/config"
    "git.callpipe.com/aidan/webmsg/sources"
    "git.callpipe.com/aidan/webmsg/storage"
)

// TODO: Code
// Teli implementation
// Signald implementation
// Info export util


func SetupLDAP() *ldap.Conn {
    srv := viper.GetString("LDAPServer")
    port := viper.GetString("LDAPPort")

    LDAP, err := ldap.DialURL("ldap://"+srv+":"+port)
    if err != nil {
        log.Log("LDAP Connect error", "LDAP")
        log.Panic(err.Error()) // dies in Panic
    }

    err = LDAP.StartTLS(&tls.Config{ServerName: srv})
    if err != nil {
        log.Log("LDAP TLS error", "LDAP")
        log.Panic(err.Error())
    }

    err = LDAP.Bind("cn=" + viper.GetString("LDAPClientUser") + "," +
                    viper.GetString("LDAPBase"),
                    viper.GetString("LDAPClientPasswd"))
    if err != nil {
        log.Log("LDAP Client Account bind failed", "LDAP")
        log.Panic(err.Error())
    }

    return LDAP
}

func mkJWTKeys() (*ecdsa.PrivateKey, *ecdsa.PublicKey, error) {
    pk, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
    if err != nil {
        return nil, nil, err
    }

    return pk, &pk.PublicKey, nil
}

func main() {
    err := config.GetViperConfig()
    if err != nil {
        err = config.SetViperDefaults()
        if err != nil {
            println("Couldnt save config")
            println(err.Error())
        }
    }

    log.InitLog()
    defer log.StopLog()

    err = storage.InitDB()
    if err != nil {
        log.Panic("Got error from db init: " + err.Error())
    }

    defer storage.TeardownDB()

    l := SetupLDAP()
    if l == nil {
        log.Panic("ldap obj is nil somehow")
    }

    defer l.Close()
    a, b, c := mkJWTKeys()
    if c != nil {
        log.Log("Couldnt gen an ECDSA key for JWT validation", "ECDSA")
        log.Panic(c.Error())
    }

    auth.SetupAuth(l, a, b)
    pages.InitTemplates()
    api.RegisterAPIHandlers()
    sources.InitializeEndpoints()

    log.Log("Calling listenAndServe", "Main")
    log.Panic(http.ListenAndServe(":8080", nil).Error())
}
