package main

import (
    "fmt"
    "git.callpipe.com/aidan/webmsg/models"
    "git.callpipe.com/aidan/webmsg/storage"
)

func user(arg string) {
    switch arg {
    case "add":
        user_add()
    case "get":
        a := user_get()
        fmt.Printf("\n%+v\n", a)
    case "update":
        user_update()
    case "delete":
        user_remove()
    }
}

func user_add() {
    var name string
    fmt.Printf("Username: ")
    fmt.Scanln(&name)
    candidate := models.User{UserID: name}

    loop := true
    for loop {
        var cid, cont string

        fmt.Printf("Enter contact id: ")
        fmt.Scanln(&cid)

        contact := contact_get(cid)
        if contact.ContactID == 0 {
            fmt.Printf("Does not exist\n")
            continue
        }

        candidate.Contacts = append(candidate.Contacts, contact)

        fmt.Printf("Enter 'y' if user owns another contact: ")
        fmt.Scanln(&cont)

        if cont != "y" {
            loop = false
        }
    }

    storage.DB.Create(&candidate)
}

func user_get() []models.User {
    id := id_get()
    var candidates []models.User
    storage.DB.Where("user_id = ?", id).Find(&candidates)

    return candidates
}

func user_update() {
    id := id_get()
    var candidate models.User

    storage.DB.Where("user_id = ?", id).First(&candidate)
    if candidate.UserID == "" {
        fmt.Println("Does not exist")
        return
    }

    indexes := []int{}
    for i, v := range candidate.Contacts {
        var choice string
        fmt.Printf("Press 'd' to remove contact %s: ", v.Username)
        fmt.Scanln(&choice)
        if choice == "d" {
            indexes = append(indexes, i)
        }
    }

    for i := len(indexes)-1; i >= 0; i--{
        candidate.Contacts = append(candidate.Contacts[:indexes[i]], 
            candidate.Contacts[indexes[i]:]...)
    }

    loop := true
    for {
        var cont string
        fmt.Printf("Key in 'y' to add a contact to this user: ")
        fmt.Scanln(&cont)

        if cont != "y" {
            loop = false
        }

        if loop {
            var cid string
            fmt.Printf("Enter contact id: ")
            fmt.Scanln(&cid)

            contact := contact_get(cid)
            if contact.ContactID == 0 {
                fmt.Printf("Does not exist\n")
                continue
            }

            candidate.Contacts = append(candidate.Contacts, contact)

        } else {
            break
        }
    }
}

func user_remove() {
    id := id_get()
    if id == "" {
        fmt.Printf("Will not delete whole table, pls specify ID")
        return
    }

    var candidate models.User
    storage.DB.Where("user_id = ?", id).First(&candidate)
    storage.DB.Delete(&candidate)
}
