package main

import (
    "fmt"
    "git.callpipe.com/aidan/webmsg/models"
    "git.callpipe.com/aidan/webmsg/storage"
)

func message(arg string) {
    switch arg {
    case "add":
        message_add()
    case "get":
        id := id_get()
        a := message_get(id)
        fmt.Printf("\n%+v\n", a)
    case "update":
        message_update()
    case "delete":
        message_remove()
    }
}

func message_add() {
    var sender, recipient int
    var content string
    fmt.Printf("Sender Contact ID: ")
    _, err := fmt.Scanln(&sender)
    if err != nil {
        fmt.Printf(err.Error())
        return
    }

    fmt.Printf("Recipient Contact ID: ")
    _, err = fmt.Scanln(&recipient)
    if err != nil {
        fmt.Printf(err.Error())
        return
    }

    fmt.Printf("Content: ")
    fmt.Scanln(&content)

    sendr := contact_get(string(sender))
    if string(sendr.ContactID) != string(sender) {
        fmt.Println("Sender does not exist!")
        return
    }

    recip := contact_get(string(recipient))
    if string(recip.ContactID) != string(recipient) {
        fmt.Println("Recipient does not exist!")
        return
    }

    candidate := models.Message{
        Sender: sendr,
        Recipients: []models.Contact{recip},
        Content: content,
    }

    storage.DB.Create(&candidate)
}

func message_get(id string) []models.Message {
    var candidates []models.Message
    storage.DB.Where("message_id = ?", id).Find(&candidates)
    return candidates
}

func message_update() {
    id := id_get()
    m := message_get(id)
    if len(m) < 1 {
        fmt.Println("Message does not exist!")
        return
    }

    var content string

    fmt.Printf("Content: ")
    fmt.Scanln(&content)

    can := m[0]
    can.Content = content
    storage.DB.Save(&can)
}

func message_remove() {
    id := id_get()
    if id == "" {
        fmt.Printf("Will not delete whole table, pls specify ID")
        return
    }

    var candidate models.Message
    storage.DB.Where("message_id = ?", id).First(&candidate)
    storage.DB.Delete(&candidate)
}
