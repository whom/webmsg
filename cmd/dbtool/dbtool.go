package main

import (
    "os"
    "fmt"
    "git.callpipe.com/aidan/webmsg/log"
    "git.callpipe.com/aidan/webmsg/config"
    "git.callpipe.com/aidan/webmsg/storage"
)

var progHelp string

func main() {
    args := os.Args[1:]
    firstOpts := []string{"get", "add", "delete", "edit"}
    secondOpts := []string{"contact", "group", "message", "user"}
    progHelp = fmt.Sprintf(
        "DBTool: apply an operation to a model contained in the webmsg database\n Operations: %v\n Models: %v",
        firstOpts, secondOpts)

    err := config.GetViperConfig()
    if err != nil {
        err = config.SetViperDefaults()
        if err != nil {
            println("Couldnt save config")
            println(err.Error())
        }
    }

    log.InitLog()
    defer log.StopLog()

    err = storage.InitDB()
    if err != nil {
        log.Panic("Got error from db init: " + err.Error())
    }

    defer storage.TeardownDB()

    log.Log("DBTool initialized successfully", "dbtool")

    if len(args) > 2 || len(args) < 2 {
        exit("Need Two Args.")
    }

    if !sliceContains(firstOpts, args[0]) {
        exit("first arg is not a valid operation")
    }

    if !sliceContains(secondOpts, args[1]) {
        exit("second arg is not a valid model")
    }

    switch args[1] {
    case "contact":
        contact(args[0])
    case "group": 
        group(args[0])
    case "message": 
        message(args[0])
    case "user": 
        user(args[0])
    }

    log.Log("DBTool exiting normally", "dbtool")
}


func exit(msg string) {
    fmt.Println(msg)
    fmt.Println(progHelp)
    log.Log("DBTool called incorrectly", "dbtool")
    os.Exit(1)
}

func sliceContains(sl []string, key string) bool {
    for _, e := range sl {
        if e == key {
            return true
        }
    }

    return false
}

func id_get() string {
    var id string
    fmt.Printf("Please enter primary key: ")
    fmt.Scanln(&id)
    return id
}
