package main

import (
    "fmt"
    "git.callpipe.com/aidan/webmsg/models"
    "git.callpipe.com/aidan/webmsg/storage"
)

func contact(arg string) {
    switch arg {
    case "add":
        contact_add()
    case "get":
        id := id_get()
        a := contact_get(id)
        fmt.Printf("\n%+v\n", a)
    case "update":
        contact_update()
    case "delete":
        contact_remove()
    }
}

func contact_add() {
    var platform, username, displayname string
    fmt.Printf("Platform: ")
    fmt.Scanln(&platform)
    fmt.Printf("Username: ")
    fmt.Scanln(&username)
    fmt.Printf("Display Name: ")
    fmt.Scanln(&displayname)

   candidate := models.Contact{
        Platform: platform,
        Username: username,
        Displayname: displayname,
    }

    storage.DB.Create(&candidate)
}

func contact_get(id string) models.Contact {
    var candidates models.Contact
    storage.DB.Where("contact_id = ?", id).First(&candidates)
    return candidates
}

func contact_update() {
    id := id_get()
    var candidate models.Contact
    storage.DB.Where("contact_id = ?", id).First(&candidate)
    fmt.Printf("Current Obj: \n%+v\n", candidate)

    var platform, username, displayname string
    fmt.Printf("Platform: ")
    fmt.Scanln(&platform)
    if platform != "" {
        candidate.Platform = platform
    }

    fmt.Printf("Username: ")
    fmt.Scanln(&username)
    if username != "" {
        candidate.Username = username
    }

    fmt.Printf("Display Name: ")
    fmt.Scanln(&displayname)
    if displayname != "" {
        candidate.Displayname = displayname
    }

    storage.DB.Save(&candidate)
}

func contact_remove() {
    id := id_get()
    if id == "" {
        fmt.Printf("Will not delete whole table, pls specify ID")
        return
    }

    var candidate models.Contact
    storage.DB.Where("contact_id = ?", id).First(&candidate)
    storage.DB.Delete(&candidate)
}
